/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * implementation for the game engine. 
 */ 

#include "Game.h" 
#include <algorithm>

using namespace std; 

Game :: Game ( string s , ostream & o , istream & i , int h , int w ): dic ( s , o ), b ( w , h ){ 
   misses = 0; 
   score = 0; 
   cout = & o; 
   cin = & i; 
} 
bool Game :: is_ended (){ 
   return misses >= 5; 
} 
void Game :: take_turn (){ 
   b.print (* cout ); 
   * cout << "Score: " << score << " Misses: " << misses << "/5\n"; 
   * cout << "Your words: "; 
   for ( int i = 0; i < used.size (); i ++){ 
   * cout << used [ i ]; 
   if ( i != used.size ()- 1 ){ 
     * cout << ", "; 
   } 
   } 
   * cout << "\nEnter a Word: "; 
   string str; 
   getline (* cin , str ); 
   while ( str.size () == 0 ){ 
   * cout << "Enter a Word: "; 
   getline (* cin , str ); 
   } 
   str = uppercase ( str ); 
   if ( check_word ( str )){ 
   score += str.size ()* str.size (); 
   used.push_back ( str ); 
   } else { 
   misses ++; 
   } 
} 
void Game :: game_over (){ 
   // * cout << "\nGame over!\nYour score was " << score << " points." << endl; 
   // * cout << "Your words: "; 
   // for ( int i = 0; i < used.size (); i ++){ 
   // * cout << used [ i ]; 
   // if ( i != used.size ()- 1 ){ 
   //   * cout << ", "; 
   // } 
   // } 
   // * cout << "\n"; 
   // ScoreList hs; 
   // hs.fillList (); 
   // if ( hs.isHigh ( Score ( "" , score , "" ))){ 
   // * cout << "You got a high score!! Please enter your name:"; 
   // string name; 
   // getline (* cin , name ); 
   // hs.addScore ( Score ( name , score , ( hs.intToString ( b.getWidth ()) + "X" + hs.intToString ( b.getHeight ())))); 
   // } 
   // * cout << "Here are the High Scores:\n"; 
   // hs.printList (* cout ); 
   // hs.writeList (); 

   // * cout << "\nDo you want to see all of the possible words from the board?(Y or N)\n"; 
   // string str; 
   // getline (* cin , str ); 
   // if ( str == "Y" || str == "y" || str == "YES" || str == "yes" ){ 
   b.print (* cout ); 
  if(1){
	* cout << "Getting all words that were on the board." << endl; 
   set<string> all = b.getAll ( dic , * cout ); 
   *cout << "there're " << all.size() << " words found." << endl;
   
   set<string>::iterator iter;
   int bestScore = 0;

   for(iter=all.begin(); iter!=all.end(); iter++)
	 {
	   string str = *iter;
	   *cout << " " + str;
	   bestScore += str.size() * str.size();
	 }
   
   *cout << "\nThe highest possible score to earn was " << bestScore; 
   } 
} 
bool Game :: check_word ( string s ){ 
   if (! dic.hasWord ( s )){ 
   * cout << "That word isn't in our dictionary!\n"; 
   return false; 
   } else if ( used_has ( s )){ 
   * cout << "You have already used that word!\n"; 
   return false; 
   } else if (! b.hasWord ( s )){ 
   * cout << "That word isn't on the board!\n"; 
   return false; 
   } else if (! b.hasBWord ( s )){ 
   * cout << "That word is on the board but requires repeating letters! (no points or misses)\n"; 
   score -= s.size ()* s.size (); 
   return true; 
   } else { 
   return true; 
   } 
} 

bool Game :: used_has ( string s ){ 
   if ( find ( used.begin (), used.end (), s ) != used.end ()){ 
   return true; 
   } 
   return false; 
}
