/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * a score value 
 */  

#ifndef SCORE_H_INCLUDED 
#define SCORE_H_INCLUDED 

#include <string> 

using namespace std; 

class Score{ 
private: 
    //represents the score reciever's name 
    string name; 
    //the score number 
    int score; 
    //the size of the board that the score was earned on 
    string size; 
public: 
    /*Pre-conditions: 
     *a string for a name, an int for a score, and a string for the board size 
     *Post-conditions: 
     *creates a score with those variables stored in it. 
     */ 
    Score(string n, int s, string si); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns the name attached to the score 
     */ 
    string getName(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns the score attached to the score 
     */ 
    int getScore(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns the size attached to the score 
     */ 
    string getSize(); 
}; 

#endif