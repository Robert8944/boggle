/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * the implementation for the list of words 
 */   
#include   "WordList.h" 

using   namespace   std ; 


WordList :: WordList ( ostream   & o ){ 
     string   filePath   =   "wordlist.txt" ; 
     ifstream   fP ; 
     fP . open ( filePath ); 
     while (! fP . eof ()){ 
         string   line ; 
         getline ( fP ,   line ); 
         list . push_back ( line ); 
     } 
     fP . close (); 
} 

WordList :: WordList ( string   s ,   ostream   & o ){ 
     o   <<   "Reading dictionary from file." ; 
     ifstream   f ; 
     f . open ( s ); 
     int   wordsRead   =   0 ; 
     int   wordsReadt   =   0 ; 
     while (! f . eof ()){ 
         string   line ; 
         getline ( f ,   line ); 
         list . push_back ( line ); 
         wordsRead ++; 
         wordsReadt ++; 
         if ( wordsRead   >   10000 ){ 
             wordsRead   -=   10000 ; 
             o   <<   "." ; 
         } 
     } 
     o   <<   " "   <<   wordsReadt   <<   " words read form file.\n\n" ; 
     f . close (); 
} 

bool   WordList :: hasWord ( string   s ){ 
     return   recHasWord ( s ,   0 ,   list . size ()- 1 ); 
} 

bool   WordList :: recHasWord ( string   s ,   int   beg ,   int   end ){ 
     if   ( end   <   beg ){ 
         return   false ; 
     } else { 
         int   mid   =   beg   +   (( end   -   beg )/ 2 ); 
         if   ( list [ mid ]. compare ( s )   >   0 ){ 
             return   recHasWord ( s ,   beg ,   mid - 1 ); 
         } else   if   ( list [ mid ]. compare ( s )   <   0 ){ 
             return   recHasWord ( s ,   mid + 1 ,   end ); 
         } else { 
             return   true ; 
         } 
     } 
} 
int   WordList :: getListSize (){ 
     return   list . size (); 
} 
string   WordList :: getWord ( int   i ){ 
     if ( i >= list . size ()   ||   i < 0 ){ 
         return   "" ; 
     } 
     return   list [ i ]; 
}

int WordList::hasPrefix(string str, int &index){
	return   hasPrefixRec ( str ,   index ,   list.size()-1 ); 
	
}

// 0: equal, >0: prefix, -1:others
int isPrefix(string pre, string str)
{
  if(pre.size() > str.size())
	return -1;

  if(pre.size() == str.size())
	if(pre == str) return 0;
	else return -1;

  for(int i = 0; i < pre.size(); i++)
	{
	  if(pre[i] != str[i])
		return -1;
	}

  return str.size() - pre.size();
}

int WordList::hasPrefixRec(string str, int &beg, int end){
  if(end < beg){
	return -1;
  }

  int mid = (beg + end) / 2;
  int cmp = list[mid].compare(str);
  if(cmp > 0){
	int ret1 = isPrefix(str, list[mid]);
	int index1 = beg;
	int ret2 = hasPrefixRec(str, beg, mid-1);
	if(ret2 != -1)
	  return ret2;
	else {
	  beg = index1;
	  return ret1;
	}
  }
  else if(cmp < 0){
	beg = mid + 1;
	return hasPrefixRec(str, beg, end);
  }
  else
	return 0;
}
