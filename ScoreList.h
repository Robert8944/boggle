/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * list of highscores 
 */  

#ifndef SCORELIST_H_INCLUDED 
#define SCORELIST_H_INCLUDED 

#include <vector> 
#include <sstream> 
#include <fstream> 
#include <iterator> 

#include "Score.h" 

using namespace std; 

class ScoreList{ 
private: 
    //a vector of the 10 highscores. 
    vector<Score> list; 
public: 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *creates a ScoreList with an empty list of scores 
     */ 
    ScoreList(); 
    /*Pre-conditions: 
     *a vector of scores 
     *Post-conditions: 
     *creates a ScoreList with a list of scores defined by l 
     */ 
    ScoreList(vector<Score> l); 
    /*Pre-conditions: 
     *a score 
     *Post-conditions: 
     *adds the score to the ordered position and removed scores untill there are ten or less in the list. 
     */ 
    void addScore(Score); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *creates a string to represent the list of high scores. 
     */ 
    string toString(); 
    /*Pre-conditions: 
     *an outstream that can be printed to 
     *Post-conditions: 
     *prints the to string to the out string 
     */ 
    void printList(ostream &o); 
    /*Pre-conditions: 
     *an integer 
     *Post-conditions: 
     *creates string to represent that integer 
     */ 
    string intToString(int i); 
    /*Pre-conditions: 
     *a stirng that represents an integer 
     *Post-conditions: 
     *creates an integer to represent that string 
     */ 
    int stringToInt(string s); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *Fills the list with scores from the "highscores.txt" file 
     */ 
    void fillList(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *writes the list to the "highscores.txt" file 
     */ 
    void writeList(); 
    /*Pre-conditions: 
     *a string 
     *Post-conditions: 
     *seprates the string into a vector seperated at each instance of "@" 
     */ 
    vector<string> split_string(string input); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *Gives the user the loswest score on the list 
     */ 
    int bottomScore(); 
    /*Pre-conditions: 
     *a score 
     *Post-conditions: 
     *returns true if the score qualifies as a high score and false if not. 
     */ 
    bool isHigh(Score s); 
}; 

#endif