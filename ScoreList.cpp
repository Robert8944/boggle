/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * the implementation for the list of scores 
 */   
#include   "ScoreList.h" 

using   namespace   std ; 

ScoreList :: ScoreList (){ 
} 

ScoreList :: ScoreList ( vector < Score >   l ){ 
     list   =   l ; 
} 
void   ScoreList :: addScore ( Score   s ){ 
     if ( list . size ()   ==   0 ){ 
         list . push_back ( s ); 
         return ; 
     } else { 
         vector < Score >:: iterator   it   =   list . begin (); 
         for ( int   i   =   0 ;   i   <   list . size ();   i ++){ 
             if ( s . getScore ()   >=   it -> getScore ()){ 
                 list . insert ( it ,   s ); 
                 while ( list . size ()   >=   11 ){ 
                     list . pop_back (); 
                 } 
                 return ; 
             } 
             it   =   next ( it ); 
         } 
         //list.insert(it, s); 
     } 
     list . push_back ( s ); 
     while ( list . size ()   >=   11 ){ 
         list . pop_back (); 
     } 
} 
string   ScoreList :: toString (){ 
     string   ans ; 
     for ( int   i   =   0 ;   i   <   list . size ();   i ++){ 
         if ( i   <   9 ){ 
             ans   +=   intToString ( i + 1 )   +   ".  "   +   list [ i ]. getName ()   +   ": "   +   intToString ( list [ i ]. getScore ())   +   "  -Size- "   +   list [ i ]. getSize ()   +   "\n" ; 
         } else { 
             ans   +=   intToString ( i + 1 )   +   ". "   +   list [ i ]. getName ()   +   ": "   +   intToString ( list [ i ]. getScore ())   +   "  -Size- "   +   list [ i ]. getSize ()   +   "\n" ; 
         } 
     } 
     return   ans ; 
} 
void   ScoreList :: printList ( ostream   & o ){ 
     o   <<   toString (); 
} 

string   ScoreList :: intToString ( int   i ){ 
     stringstream   ss ; 
     ss   <<   i ; 
     return   ss . str (); 
} 
int   ScoreList :: stringToInt ( string   s ){ 
     return   stoi ( s ); 
} 
void   ScoreList :: fillList (){ 
     ifstream   iF ; 
     iF . open ( "highscores.txt" ); 
     if ( iF . fail ()){ 
         iF . clear (); 
         return ; 
     } else   { 
         while (! iF . eof ()){ 
             string   str ; 
             getline ( iF ,   str ); 
             if ( str != "" ){ 
                 vector < string >   temp   =   split_string ( str ); 
                 Score   s ( temp [ 0 ],   stoi ( temp [ 1 ]),   temp [ 2 ]); 
                 list . push_back ( s ); 
             } 
         } 
     } 
} 
vector < string >   ScoreList :: split_string ( string   input ){ 
     vector < string >   pieces ; 
     int   field_start   =   0 ; 
     int   next_at ; 
     do   { 
         next_at   =   input . find ( '@' ,   field_start ); 
         if   ( next_at   ==   string :: npos ) 
             pieces . push_back ( input . substr ( field_start )); 
         else 
             pieces . push_back ( input . substr ( field_start ,   next_at   -   field_start )); 
         field_start   =   next_at   +   1 ; 
     }   while ( next_at   !=   string :: npos ); 
     return   pieces ; 
} 

void   ScoreList :: writeList (){ 
     ofstream   of ; 
     of . open ( "highscores.txt" ); 
     for ( int   i   =   0 ;   i   <   list . size ();   i ++){ 
         string   temp   =   ( list [ i ]. getName ()   +   "@"   +   intToString ( list [ i ]. getScore ())   +   "@"   +   list [ i ]. getSize ()   +   "\n" ); 
         of   <<   temp ; 
     } 
} 
int   ScoreList :: bottomScore (){ 
     if ( list . size ()>= 1 ) 
         return   list [ list . size ()- 1 ]. getScore (); 
     else 
         return   0 ; 
} 
bool   ScoreList :: isHigh ( Score   s ){ 
     return   ( list . size ()   <   10   ||   s . getScore ()   >=   bottomScore ()); 
}