/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * the implementation for the score 
 */   
#include   "Score.h" 

using   namespace   std ; 

Score :: Score ( string   n ,   int   s ,   string   si ){ 
     name   =   n ; 
     score   =   s ; 
     size   =   si ; 
} 
string   Score :: getName (){ 
     return   name ; 
} 
int   Score :: getScore (){ 
     return   score ; 
} 
string   Score :: getSize (){ 
     return   size ; 
}