/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * Driver program for WordGrid game. 
 */   

#include   <iostream> 
#include   <time.h> 
#include   "Game.h" 
using   namespace   std ; 

void   pause_215 ( bool   have_newline ); 

int   main () 
{ 
     // TODO: seed the RNG 

     srand   ( time ( NULL )); 

     cout   <<   "WordGrid by Robert McGillivray - CS 215 PA4\n" ; 

     cout   <<   "What size sould the board be?\nWidth: " ; 
     int   tx ; 
     cin   >>   tx ; 
     while ( cin . fail ()   ||   tx   <   2 ){ 
         cout   <<   "I need a number please greater than 1 please: " ; 
         cin . clear (); 
         cin . ignore ( 200 ,   '\n' ); 
         cin   >>   tx ; 
     } 
     cout   <<   "Height: " ; 
     int   ty ; 
     cin   >>   ty ; 
     while ( cin . fail ()   ||   ty   <   2 ){ 
         cout   <<   "I need a number please greater than 1 please: " ; 
         cin . clear (); 
         cin . ignore ( 200 ,   '\n' ); 
         cin   >>   ty ; 
     } 
     cin . ignore ( 200 ,   '\n' ); 
     Game   game ( "wordlist.txt" ,   cout ,   cin ,   ty ,   tx ); 

     // while   (! game . is_ended ()){ 
     //     game . take_turn (); 
     // } 
     game . game_over (); 


     pause_215 ( false ); 
     return   0 ; 
} 

void   pause_215 ( bool   have_newline ) 
{ 
     if   ( have_newline )   { 
         // Ignore the newline after the user's previous input. 
         cin . ignore ( 200 ,   '\n' ); 
     } 

     // Prompt for the user to press ENTER, then wait for a newline. 
     cout   <<   endl   <<   "Press ENTER to continue."   <<   endl ; 
     cin . ignore ( 200 ,   '\n' ); 
} 
