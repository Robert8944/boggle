/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * Holds all letters and their formations 
 */  

#ifndef BOARD_H_INCLUDED 
#define BOARD_H_INCLUDED 

#include <string> 
#include <vector> 
#include <map>
#include <set>

#include "letters.h" 
#include "WordList.h" 

using namespace std; 

class Board{ 
private: 
    //a 2D vector representing all of the letters in the board 
    vector<vector<int>> board; 
	//a map with the a key for each letter and the values returned are the locations of those letters
	map<string, vector<vector<int>>> boardMap;
    //an integer for the heigth and width of the board. 
    int width, height; 

    /*Pre-conditions: 
     *a string s to be found, an int x and y to show the place to be checked, a position in the string s to show the char were looking for, and a vector of our previously visited x and y coordinates 
     *Post-conditions: 
     *checks to see if the word is on the board according to boggle rules recursivley. 
     */ 
    int hasBWordRec(string s, int x, int y, int pos, vector<vector<int>> prev); 
    /*Pre-conditions: 
     *a string s to be found, an int x and y to show the place to be checked, and a position in the string s to show the char were looking for 
     *Post-conditions: 
     *checks to see if the word is on the board at all recursivley. 
     */ 
    int hasWordRec(string s, int x, int y, int pos); 
public: 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *creates a 4X4 board 
     */ 
    Board(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *creates a board with the given heigth and width 
     */ 
    Board(int w, int h); 
    /*Pre-conditions: 
     *an ostream that we can print to 
     *Post-conditions: 
     *prints the board to the given ostream 
     */ 
    void print(ostream &o); 
    /*Pre-conditions: 
     *a string s to be found 
     *Post-conditions: 
     *calls on hasBWordRec for every location on the board to see if the board contains the string s according to boggle rules. 
     *returns true if it does and false if it doesnt. 
     */ 
    int hasBWord(string s); 
    /*Pre-conditions: 
     *a string s to be found 
     *Post-conditions: 
     *calls on hasWordRec for every location on the board to see if the board contains the string s. 
     *returns true if it does and false if it doesnt. 
     */ 
    int hasWord(string s); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns the height. 
     */ 
    int getHeight(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns the width 
     */ 
    int getWidth(); 
    /*Pre-conditions: 
     *a vector and a vector of vectors 
     *Post-conditions: 
     *returns true if the vector of vectors contains the vector v as one of it' vectors and false if it doesn't. 
     */ 
    bool vecHas(vector<int> v, vector<vector<int>> vt); 
    /*Pre-conditions: 
     *a wordlist to check the words from and a ostream to print to 
     *Post-conditions: 
     *returns a vecotr of all of the strings fron the given wordlist that were in the board. 
     */ 
    set<string> getAll(WordList &d, ostream &o); 

	void getAllRec(set<string> &foundWords, WordList &d, int i, int j, string str, int index);
}; 
#endif
