/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * Helps play the game and take turns 
 */  

#ifndef GAME_H_INCLUDED 
#define GAME_H_INCLUDED 

#include <string> 
#include <vector> 
#include <iostream>

#include "WordList.h" 
#include "Board.h" 
#include "ScoreList.h" 

using namespace std; 

class Game{ 
private: 
    //a wordlist used for the dictionary 
    WordList dic; 
    // a board used to represent the game 
    Board b; 
    //the number of misses 
    int misses; 
    //the score of the user 
    int score; 
    //a vector listing the words sucessfully entered 
    vector<string> used; 
    //an ostream the game will write to 
    ostream *cout; 
    //an istream the game will get input from 
    istream *cin; 
public: 
    /*Pre-conditions: 
     *a string, an input and output stream, and a desired height and width. 
     *Post-conditions: 
     *creates a game with the given parameters. 
     */ 
    Game(string s, ostream &o, istream &i, int h, int w); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns true if the game is over and false if not 
     */ 
    bool is_ended(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *outputs to the given ostream and asks for input on the given istream, plays a turn through these. 
     */ 
    void take_turn(); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *runs the sequence for when a game is over using the given istream an ostream 
     */ 
    void game_over(); 
    /*Pre-conditions: 
     *s string 
     *Post-conditions: 
     *checks to see if the word should award points or a miss. checks Boggle rules, non-boggle rules, the previous words, and the dictionary. 
     */ 
    bool check_word(string s); 
    /*Pre-conditions: 
     *s string 
     *Post-conditions: 
     *checks to see if the word has been used sucsussfully been used on the board 
     */ 
    bool used_has(string s); 
}; 
#endif
