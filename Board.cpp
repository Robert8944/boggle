/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * _ November 21, 2013 
 * 
 * implementation for the game board 
 */ 

#include <set>

#include "Board.h" 

using namespace std; 

Board :: Board (){ 
  width = 4; 
  height = 4; 
  for ( int i = 0; i < height; i ++){ 
	vector < int > temp; 
	for ( int j = 0; j < width; j ++){ 
	  temp . push_back ( random_letter ()); 
	} 
	board . push_back ( temp ); 
  } 
} 
Board :: Board ( int w , int h ){ 
  width = w; 
  height = h; 
  for ( int i = 0; i < h; i ++){ 
	vector < int > temp; 
	for ( int j = 0; j < w; j ++){ 
	  temp . push_back ( random_letter ()); 
	} 
	board . push_back ( temp ); 
  } 
} 
int Board :: hasWord ( string s ){ 
  int ans = 0; 
  for ( int i = 0; i < height; i ++){ 
	for ( int j = 0; j < width; j ++){ 
	  if ( hasWordRec ( s , i , j , 0 ) == 1 ){ 
		ans = 1; 
	  } 
	} 
  } 
  return ans; 
} 
int Board :: hasWordRec ( string s , int x , int y , int pos ){ 
  if ( pos == s . size ()){ 
	return 1; 
  } 
  if ( x >= board . size () || y >= board [ 0 ]. size ()){ 
	return 0; 
  } 

  int ans = 0; 
  for ( int i = - 1; i <= 1; i ++){ 
	for ( int j = - 1; j <= 1; j ++){ 
	  if (!( i == 0 && j == 0 )){ 
		if ( s [ pos ] == board [ x ][ y ] && hasWordRec ( s , x + i , y + j , pos + 1 ) == 1 ){ 
		  ans = 1; 
		} 
	  } 
	} 
  } 
  return ans; 
} 
int Board :: hasBWord ( string s ){ 
  int ans = 0; 
  for ( int i = 0; i < height; i ++){ 
	for ( int j = 0; j < width; j ++){ 
	  vector < vector < int >> prev; 
	  if ( hasBWordRec ( s , i , j , 0 , prev ) == 1 ){ 
		ans = 1; 
	  } 
	} 
  } 
  return ans; 
} 
bool Board :: vecHas ( vector < int > v , vector < vector < int >> vt ){ 
  for ( int j = 0; j < vt . size (); j ++){ 
	if ( vt [ j ][ 0 ] == v [ 0 ] && vt [ j ][ 1 ] == v [ 1 ]){ 
	  return true; 
	} 
  } 
  return false; 
} 
int Board :: hasBWordRec ( string s , int x , int y , int pos , vector < vector < int >> prev ){ 
  /*if(find(prevX.begin(), prevX.end(), x) != prevX.end() && find(prevY.begin(), prevY.end(), y) != prevY.end() && distance(prevY.begin(), find(prevY.begin(), prevY.end(), y)) == distance(prevX.begin(), find(prevX.begin(), prevX.end(), x))){ 
	return 0; 
  }*/ 
  vector < int > v; 
  v . push_back ( x ); 
  v . push_back ( y ); 
  if ( vecHas ( v , prev )){ 
	return 0; 
  } 
  prev . push_back ( v ); 
  if ( pos == s . size ()){ 
	return 1; 
  } 
  if ( x >= board . size () || y >= board [ 0 ]. size ()){ 
	return 0; 
  } 

  int ans = 0; 
  for ( int i = - 1; i <= 1; i ++){ 
	for ( int j = - 1; j <= 1; j ++){ 
	  if ( s [ pos ] == board [ x ][ y ] && hasBWordRec ( s , x + i , y + j , pos + 1 , prev ) == 1 ){ 
		ans = 1; 
	  } 
	} 
  } 
  return ans; 
} 
void Board :: print ( ostream & o ){ 
  string ans; 
  for ( int i = 0; i < height; i ++){ 
	for ( int j = 0; j < width; j ++){ 
	  ans += "+---"; 
	} 
	ans += "+\n"; 
	for ( int k = 0; k < width; k ++){ 
	  ans += "| "; 
	  ans += board [ i ][ k ]; 
	  ans += " "; 
	} 
	ans += "|\n"; 
  } 
  for ( int j = 0; j < width; j ++){ 
	ans += "+---"; 
  } 
  ans += "+\n"; 
  o << ans; 
   
} 
int Board :: getHeight (){ 
  return height; 
} 
int Board :: getWidth (){ 
  return width; 
} 
set < string > Board :: getAll ( WordList &d , ostream & o ){ 
  set<string>  ans; 

  for (int i = 0; i < height; i++){
	for (int j = 0; j < width; j++){
	  getAllRec(ans, d, i, j, "", 0);
	}
  }
  
  return ans; 
}
void Board::getAllRec(set<string> &foundWords, WordList &d, int x, int y, string str, int index)
{
  str = str + char(board[x][y]);
  int temp = index;
  int val = d.hasPrefix(str, index);

  if(val < 0)
	{ // not prefix
	  index = temp;
	  return;
	}
  else if(val == 0)
	{
	  foundWords.insert(str);
	}
  
  for (int i = x-1; i <= x+1; i++)
	{
	  if(i<0 || i>=height)
		continue;

	  for (int j = y-1; j <= y+1; j++)
		{
		  if((i==0 && j==0) || (j<0 || j>=width))
			continue;

		  getAllRec(foundWords, d, i, j, str, index);
		}
	}
}
