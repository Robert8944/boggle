/* Name Robert McGillivray 
 * CS 215, Fall 2013 
 * Programming Assignment 4 - http://www.cs.uky.edu/~neil/215/pa/4/ 
 * _ November 21, 2013 
 * 
 * list for the dictionary. 
 */  

#ifndef WORDLIST_H_INCLUDED 
#define WORDLIST_H_INCLUDED 

#include <string> 
#include <vector> 
#include <fstream> 
#include <cmath> 
using namespace std; 

class WordList{ 
private: 
    /* 
     *a list that contains all of the words in the dictionary. 
     */ 
    vector<string> list; 

    /*Pre-conditions: 
     *the string the user wants to look for and the start and stop points in the word list to check 
     *Post-conditions: 
     *returns false if the boolean wasnt found in that section and true if it was. 
     */ 
    bool recHasWord(string s, int beg, int end); 
public: 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *creates a WordList filled with the words in the default file location (wordlist.txt) 
     */ 
    WordList(ostream &o); 
    /*Pre-conditions: 
     *a string that is assumed to be correct 
     *Post-conditions: 
     *creates a WordList filled with the words in the file connected to the given ifstream 
     */ 
    WordList(string s, ostream &o); 
    /*Pre-conditions: 
     *a string that the user wants to check if it is contained in the wordlist 
     *Post-conditions: 
     *returns true if it is contained and false if not. 
     */ 
    bool hasWord(string s); 
    /*Pre-conditions: 
     *N/A 
     *Post-conditions: 
     *returns the size of the WordList 
     */ 
    int getListSize(); 
    /*Pre-conditions: 
     *an integer for an index in the list 
     *Post-conditions: 
     *returns the string at the given index in the list 
     */ 
    string getWord(int i); 

	int hasPrefix(string str, int &index);

	int hasPrefixRec(string str, int &index, int end);
}; 
#endif